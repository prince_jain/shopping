// export default [
//     {
//         id: 1,
//         title: "Samsung Galaxy S7",
//         price: 700,
//         img: "https://res.cloudinary.com/drecbsopp/image/upload/v1627398399/samasung-galaxy-a51-8gb-8uh_tndbgv.jpg",
//         quantity: 1,
//     },
//     {
//         id: 2,
//         title: "Moto G5 plus",
//         price: 600,
//         img: "https://res.cloudinary.com/drecbsopp/image/upload/v1627398477/MotoGPowerDual_2021_Reformatted_1_330x_wp8gve.png",
//         quantity: 1,
//     },
//     {
//         id: 3,
//         title: "Xiaomi Redmi Note 2",
//         price: 500,
//         img: "https://res.cloudinary.com/drecbsopp/image/upload/v1627398543/D7A7DA95-AEF8-228B-A2D2-A3FEBF237C33_y9p6wq.png",
//         quantity: 1,
//     },
//     {
//         id: 4,
//         title: "Nokia G20",
//         price: 400,
//         img: "https://res.cloudinary.com/drecbsopp/image/upload/v1627398698/nokia3_resize_md_qdw7bv.jpg",
//         quantity: 1,
//     },
//     {
//         id: 5,
//         title: "Iphone-14 plus",
//         price: 700,
//         img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
//         quantity: 1,
//     },
//     {
//         id: 6,
//         title: "Redmi Note 12 Pro",
//         price: 1000,
//         img: "https://i.gadgets360cdn.com/products/large/redmi-note-12-5g-pro-plus-db-gadgets360-800x600-1673019783.jpg" ,
//         quantity: 1,
//     },
//     {
//         id: 7,
//         title: "IQOO Neo 7",
//         price: 1200,
//         img: "https://www.91-img.com/gallery_images_uploads/3/d/3df5ca6a9b470f715b085991144a5b76e70da975.JPG?tr=h-550,w-0,c-at_max" ,
//         quantity: 1,
//     },
//     {
//         id: 8,
//         title: "Vivo T2x",
//         price: 300,
//         img: "https://www.91-img.com/gallery_images_uploads/3/d/3df5ca6a9b470f715b085991144a5b76e70da975.JPG?tr=h-550,w-0,c-at_max" ,
//         quantity: 1,
//     },
//     {
//         id: 9,
//         title: "Xiaomi 13 Pro",
//         price: 900,
//         img: "https://www.91-img.com/gallery_images_uploads/3/d/3df5ca6a9b470f715b085991144a5b76e70da975.JPG?tr=h-550,w-0,c-at_max" ,
//         quantity: 1,
//     },
//     {
//         id: 10,
//         title: "OnePlus Nord 2T 5G",
//         price: 900,
//         img: "https://www.91-img.com/gallery_images_uploads/3/d/3df5ca6a9b470f715b085991144a5b76e70da975.JPG?tr=h-550,w-0,c-at_max" ,
//         quantity: 1,
//     },
// ];


export default [
    {
        id: 1,
        title: "Samsung Galaxy S7",
        price: 700,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 2,
        title: "Moto G5 plus",
        price: 600,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 3,
        title: "Xiaomi Redmi Note 2",
        price: 500,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 4,
        title: "Nokia G20",
        price: 400,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 5,
        title: "Iphone-14 plus",
        price: 700,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 6,
        title: "Redmi Note 12 Pro",
        price: 1000,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 7,
        title: "IQOO Neo 7",
        price: 1200,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 8,
        title: "Vivo T2x",
        price: 300,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 9,
        title: "Xiaomi 13 Pro",
        price: 900,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
    {
        id: 10,
        title: "OnePlus Nord 2T 5G",
        price: 900,
        img: "https://www.luluhypermarket.com/cdn-cgi/image/f=auto/medias/2032175-02.jpg-1200Wx1200H?context=bWFzdGVyfGltYWdlc3w5MDY2NHxpbWFnZS9qcGVnfGFESTVMMmd4WkM4eE5UQTVOVEUxTURReE1UZ3dOaTh5TURNeU1UYzFMVEF5TG1wd1oxOHhNakF3VjNneE1qQXdTQXw5YzUxYTdjOWM1NmU0YWFiYmUxNjkxMzRjYmZiY2EzYzA0YzAwNTVkYzI0NGI3NjlhZDVjOTExMDk0NjNiMTUx" ,
        quantity: 1,
    },
];