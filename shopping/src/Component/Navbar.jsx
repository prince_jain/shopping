import React from "react";
import {
  MDBContainer,
  MDBNavbar,
  MDBNavbarBrand,
  MDBBtn,
} from "mdb-react-ui-kit";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getCartTotal } from "../features/cartSlice";
import { useEffect } from "react";
import Login from "./Login";

export default function App() {

  const { cart, totalQuantity } = useSelector((state) => state.allCart); 

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCartTotal());
  }, [cart]);

  return (
    <MDBNavbar style={{backgroundColor:"#282c34"}}>
      <MDBContainer fluid>
          <MDBNavbarBrand style={{color: "lightblue"}}>E-Cart</MDBNavbarBrand>
          <span>
            <Link to="/">All Product</Link>  
          </span>
          <span>
            <input type="text" placeholder="Search E-Cart.com" style={{width: "450px",height: "40px", borderColor: "darkgray"}}/>
            <button type="submit" style={{height: "39px", color: "gray", border: "1px", width: 45}}><i class="fa fa-search" ></i></button>
          </span>
          <span>
            <Link to="/login">Login</Link>
          </span>
          <MDBBtn color="dark">
           <Link to="/cart">Cart({totalQuantity})</Link> 
          </MDBBtn>
      </MDBContainer>
    </MDBNavbar>
  );
}
