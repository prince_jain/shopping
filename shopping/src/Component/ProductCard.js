import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCardImage,
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
} from "mdb-react-ui-kit";
import { useSelector, useDispatch } from "react-redux";
import { addToCart } from "../features/cartSlice";

export default function App() {
  const items = useSelector((state) => state.allCart.items);

  const dispatch = useDispatch();

  return (
    <>
     <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" height="350px" src="https://images-eu.ssl-images-amazon.com/images/G/31/img23/Fashion/Event/AF/PD/Teaser/Ingresses/v1/PC_Eng_Rec.gif" alt="First slide"></img>
    </div>
  </div>
</div>

      <div className="m-2">
        <MDBContainer>
          <MDBRow className="m-3">
            {items.map((item) => (
              <MDBCol key={item.id} size="3">
                <MDBCard
                  style={{
                    display: "inline-block",
                    width: "250px",
                    height: "500px",
                    marginTop: 60,
                  }}
                >
                  <MDBCardImage
                    src={item.img}
                    position="top"
                    alt="..."
                    style={{ height: "300px" }}
                  />
                  <MDBCardBody>
                    <MDBCardTitle>{item.title}</MDBCardTitle>
                    <MDBCardText>Price : ${item.price}</MDBCardText>
                    <MDBBtn onClick={() => dispatch(addToCart(item))}>
                      Add To Cart
                    </MDBBtn>
                  </MDBCardBody>
                </MDBCard>
              </MDBCol>
            ))}
          </MDBRow>
        </MDBContainer>
      </div>
    </>
  );
}
