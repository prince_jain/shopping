import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Navbar from "./Component/Navbar";
import ProductCard from "./Component/ProductCard";
import CartPage from "./Component/cartPage";
import Login from "./Component/Login";
import Registration from "./Component/Registration";

function App() {
  return (
    <BrowserRouter>
      <div>
        <Navbar />
        <Routes>
          <Route exact path="/" element={<ProductCard/>}/>
          <Route path="/cart" element={<CartPage/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/registration" element={<Registration/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
